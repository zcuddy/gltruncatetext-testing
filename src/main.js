import Vue from 'vue'
import setConfigs from '@gitlab/ui/dist/config'
setConfigs()

import App from './App.vue'

import './assets/main.css'

new Vue({
  render: (h) => h(App)
}).$mount('#app')
