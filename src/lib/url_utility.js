/**
 * Returns true if url is an absolute URL
 *
 * @param {String} url
 */
export function isAbsolute(url) {
  return /^https?:\/\//.test(url);
}

/**
 * Returns current base URL
 */
export function getBaseURL() {
  const { protocol, host } = window.location;
  return `${protocol}//${host}`;
}

/**
 * Returns a normalized url
 *
 * https://gitlab.com/foo/../baz => https://gitlab.com/baz
 *
 * @param {String} url - URL to be transformed
 * @param {String?} baseUrl - current base URL
 * @returns {String}
 */
export const getNormalizedURL = (url, baseUrl) => {
  const base = baseUrl || getBaseURL();
  try {
      return new URL(url, base).href;
  } catch (e) {
      return '';
  }
};

/**
 * Converts a relative path to an absolute or a root relative path depending
 * on what is passed as a basePath.
 *
 * @param {String} path       Relative path, eg. ../img/img.png
 * @param {String} basePath   Absolute or root relative path, eg. /user/project or
 *                            https://gitlab.com/user/project
 */
export function relativePathToAbsolute(path, basePath) {
  const absolute = isAbsolute(basePath);
  const base = absolute ? basePath : `file:///${basePath}`;
  const url = new URL(path, base);
  url.pathname = url.pathname.replace(/\/\/+/g, '/');
  return absolute ? url.href : decodeURIComponent(url.pathname);
}